"""
Provides functions to add noise onto a geographical point to guarantee differential privacy defined by the
privacy budget epsilon. Typical epsilon values lie within (0,3] (a lower epsilon corresponds to higher privacy).
The algorithm is implemented according to the paper:
    Andrés, M. et al. (2013) ‘Geo-Indistinguishability : Differential Privacy for Location-Based Systems’,
    Proceedings of the 2013 ACM SIGSAC conference on Computer and communications security, pp. 901–914.
    Available at: https://arxiv.org/pdf/1212.1984v3.pdf.
Noise is added to the longitude, latitude representation of a geographical location. The formulas for calculating
the perturbed location are derived from http://www.movable-type.co.uk/scripts/latlong.html
"""

import numpy as np
from scipy.special import lambertw

from de4l_geodata.geodata.point import Point


def calculate_noise_parameters(epsilon):
    """
    Calculates random distance and angle of noise perturbation depending on the privacy budget epsilon.
    See Andrés, M. et al. (2013), Figure 3 on page 7.

    Parameters
    ----------
    epsilon : float
        The privacy budget used to calculate the noise parameters.

    Returns
    -------
    Tuple
        The distance and angle of the noise perturbation vector.
    """
    theta = np.random.uniform(0, 2 * np.pi)  # angle in radian
    probability = np.random.uniform(0, 1)
    radius = (-1 / epsilon) * (lambertw((probability - 1) / np.e, -1) + 1).real
    return radius, theta


def perturb_(location, epsilon):
    """
    Adds two-dimensional noise onto a geographical location and modifies the location instantly.
    See Andrés, M. et al. (2013), Figure 6 on page 8.

    Parameters
    ----------
    location : Point
        Geographical location, specified by a latitude and longitude in radian.
    epsilon : float
        The privacy budget used to calculate the noise parameters.
    """
    if not isinstance(location, Point):
        raise TypeError('location should be of type geodata.point.Point.')
    radius, theta = calculate_noise_parameters(epsilon)
    location.add_vector_(radius, theta)


def perturb(location, epsilon):
    """
    Adds two-dimensional noise onto a geographical location without modifying the input location.
    See Andrés, M. et al. (2013), Figure 6 on page 8.

    Parameters
    ----------
    location : Point
        Geographical location, specified by a latitude and longitude in radian.
    epsilon : float
        The privacy budget used to calculate the noise parameters.

    Returns
    -------
    location : Point
        The perturbed location.
    """
    if not isinstance(location, Point):
        raise TypeError('location should be of type geodata.point.Point.')
    radius, theta = calculate_noise_parameters(epsilon)
    return location.add_vector(radius, theta)
