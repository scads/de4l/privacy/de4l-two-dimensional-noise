"""
Provides functions to add noise onto a trajectory consisting of correlated geographical points to guarantee differential
privacy defined by the privacy budget epsilon. Typical epsilon values lie within (0,3] (a lower epsilon corresponds to
higher privacy).
The algorithm is implemented according to the paper:
    Jiang, K. et al. (2013) ‘Publishing trajectories with differential privacy guarantees’,
    SSDBM: Proceedings of the 25th International Conference on Scientific and Statistical Database Management, pp. 1–12.
    Available at: https://dl.acm.org/doi/10.1145/2484838.2484846.
A noise vector is drawn from a sphere and added to the longitude, latitude pairs of a trajectory. The formulas for
calculating he perturbed location are derived from http://www.movable-type.co.uk/scripts/latlong.html.
"""

import numpy as np

from de4l_geodata.geodata.point_t import PointT
from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point import Point
from de4l_geodata.geodata.point import get_distance


def sphere_sampling(dimension, radius):
    """
    Generate uniformly distributed points from the (n−1)-sphere of the given radius, as described in
        M. Muller (1959) ’A note on a method for generating points uniformly on n-dimensional spheres’
        Communications of the ACM, 2(4):19–20

    Parameters
    ----------
    dimension : int
        The number of points to draw.
    radius : int
        The radius of the noise sphere.

    Returns
    -------
    sampled :
        An array of n uniformly distributed points on the (n−1)-sphere of the given radius.
    """
    radius = max(radius, 0)
    probabilities = np.random.normal(0, 1, dimension)
    weight = np.sqrt(np.sum([np.power(x, 2) for x in probabilities]))
    sampled = [radius / weight * x for x in probabilities]
    return sampled


def perturb_(trajectory, epsilon=0.1, delta=0.9):
    """
    Adds global noise onto a trajectory consisting of correlated geographical points and modifies the trajectory
    instantly to achieve (epsilon, delta)-DP. See Jiang, K. et al. (2013), Algorithm 2 on page 5.

    See Andrés, M. et al. (2013), Figure 6 on page 8.

    Parameters
    ----------
    trajectory : Route
        The trajectory, specified by points in radians.
    epsilon : float
        The privacy budget.
    delta : float
        The allowed delta, with which privacy will not hold.
    """
    if not isinstance(trajectory, Route):
        raise TypeError('trajectory should be of type geodata.route.Route.')
    trajectory = perturb(trajectory, epsilon, delta)


def perturb(trajectory, epsilon=0.1, delta=0.9):
    """
    Adds global noise onto a trajectory consisting of correlated geographical points without modifying the input
    trajectory to achieve (epsilon, delta)-DP. See Jiang, K. et al. (2013), Algorithm 2 on page 5.

    Parameters
    ----------
    trajectory : Route
        The trajectory, specified by points in radians.
    epsilon : float
        The privacy budget.
    delta : float
        The allowed delta, with which privacy will not hold.

    Returns
    -------
    Route
        The perturbed trajectory.
    """
    if not isinstance(trajectory, Route):
        raise TypeError('trajectory should be of type geodata.route.Route.')
    is_radians = trajectory.get_coordinates_unit() == 'radians'
    is_cartesian = trajectory.get_geo_reference_system() == 'cartesian'
    trajectory.to_radians_(ignore_warnings=True)
    trajectory.to_cartesian_(ignore_warnings=True)

    # We assume that the distance between any two consecutive points in the trajectory is bounded by a constant M
    max_distance = 0
    for i in range(1, len(trajectory)):
        distance = get_distance(trajectory[i - 1], trajectory[i])
        if distance > max_distance:
            max_distance = distance

    # GNoise is (ε, δ)-differentially private when b ≥ 2M/ε + 2M/ε · (2n − 1) · (ln 1/(1−δ))^−1 where M is max_distance
    dimension = len(trajectory) - 2
    if delta == 1:
        scale_parameter = 2. * max_distance / epsilon
    else:
        scale_parameter = 2. * max_distance / epsilon \
            + (2. * max_distance / epsilon) * (2. * dimension - 1.) * np.power(np.log(1. / (1. - delta)), -1)
    noisy_radius = np.random.exponential(scale_parameter)
    noise = sphere_sampling(2 * dimension, noisy_radius)
    perturbed_trajectory = trajectory.deep_copy()
    for i in range(dimension):
        perturbed_point = Point([perturbed_trajectory[i + 1].x_lon + noise[2 * i],
                                 perturbed_trajectory[i + 1].y_lat + noise[2 * i + 1]],
                                geo_reference_system='cartesian',
                                coordinates_unit=trajectory.get_coordinates_unit())
        if perturbed_trajectory.has_timestamps():
            perturbed_point = PointT(perturbed_point,
                                     timestamp=perturbed_trajectory[i + 1].timestamp,
                                     geo_reference_system='cartesian',
                                     coordinates_unit=trajectory.get_coordinates_unit())
        perturbed_trajectory[i + 1] = perturbed_point

    if not is_cartesian:
        trajectory.to_latlon_()
        perturbed_trajectory.to_latlon_()
    if not is_radians:
        trajectory.to_degrees_()
        perturbed_trajectory.to_degrees_()
    return perturbed_trajectory
