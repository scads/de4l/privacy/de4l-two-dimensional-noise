# de4l-two-dimensional-noise

<h1>Privacy algorithm for geolocation</h1>
<p>This algorithm adds noise onto a geographical point to guarantee geo-indistinguishability based on differential privacy defined by the privacy budget epsilon.</p>

Install this package via pip:
```bash
pip install git+https://git@git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-two-dimensional-noise.git
```