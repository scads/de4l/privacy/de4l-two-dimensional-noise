"""Test two-dimensional-noise algorithm.
"""

import unittest

from de4l_geodata.geodata.point import Point

from de4l_two_dimensional_noise.privacy_algorithm import noise2d


class TestNoise2DMethods(unittest.TestCase):
    """Test two-dimensional-noise algorithm.
    """

    def test_perturb_(self):
        """Test perturbation function of two-dimensional-noise algorithm.
        """
        epsilon = 1
        point = Point([1, 1], geo_reference_system='cartesian')
        point.to_latlon_()
        point_copy = point.deep_copy()
        noise2d.perturb_(point, epsilon)
        self.assertNotEqual(point, point_copy)

    def test_perturb(self):
        """Test perturbation function of two-dimensional-noise algorithm.
        """
        epsilon = 1
        point = Point([1, 1], geo_reference_system='cartesian')
        point.to_latlon_()
        perturbed_point = noise2d.perturb(point, epsilon)
        self.assertNotEqual(point, perturbed_point)


if __name__ == '__main__':
    unittest.main()
