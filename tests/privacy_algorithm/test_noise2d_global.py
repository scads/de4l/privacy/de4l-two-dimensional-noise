"""Test global two-dimensional-noise algorithm.
"""

import unittest

from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point_t import PointT
import pandas as pd

from de4l_two_dimensional_noise.privacy_algorithm import noise2d_global


class TestNoise2DGlobalMethods(unittest.TestCase):
    """Test two-dimensional-noise algorithm.
    """
    def setUp(self) -> None:
        self.route = Route([[12.38003393341282, 51.33633113893831], [12.37614967936791, 51.335724465304054],
                            [12.371901276506291, 51.33538320785665], [12.370626755647805, 51.33538320785665],
                            [12.370626755647805, 51.33799945003596]], coordinates_unit='degrees')
        self.route_with_timestamps = Route([
            PointT([12.38003393341282, 51.33633113893831], timestamp=pd.Timestamp(0), coordinates_unit='degrees'),
            PointT([12.37614967936791, 51.335724465304054], timestamp=pd.Timestamp(1), coordinates_unit='degrees'),
            PointT([12.371901276506291, 51.33538320785665], timestamp=pd.Timestamp(2), coordinates_unit='degrees'),
            PointT([12.370626755647805, 51.33538320785665], timestamp=pd.Timestamp(3), coordinates_unit='degrees'),
            PointT([12.370626755647805, 51.33799945003596], timestamp=pd.Timestamp(4), coordinates_unit='degrees')
        ])

    def test_perturb(self):
        """Test perturbation function of global two-dimensional-noise algorithm.
        """
        epsilon = 0.1
        delta = 0.9

        route_copy = self.route.deep_copy()
        route_copy = noise2d_global.perturb(route_copy, epsilon, delta)
        self.assertNotEqual(self.route, route_copy)

    def test_perturb_(self):
        """Test instant perturbation function of global two-dimensional-noise algorithm.
        """
        epsilon = 0.1
        delta = 0.9
        route_copy = self.route.deep_copy()
        noise2d_global.perturb_(route_copy, epsilon, delta)
        self.assertNotEqual(self.route, route_copy)

        self.assertTrue(noise2d_global.perturb(self.route_with_timestamps).has_timestamps())
